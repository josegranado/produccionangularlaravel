<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Image;
use Illuminate\Support\Facades\Response;
class ImagesController extends Controller
{

    public function index()
    {
        $images = Image::all();
        $response = Response::json($images, 200);
        return $response;
    }

   
    public function create()
    {
        
    }

    
    public function store(Request $request)
    {
        
    }

    
    public function show($id)
    {
        
    }


    public function edit($id)
    {
       
    }

 
    public function update(Request $request, $id)
    {
        
    }


    public function destroy($id)
    {

    }
}
